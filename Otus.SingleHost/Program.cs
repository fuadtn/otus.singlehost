var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllersWithViews();
builder.Services.AddCors();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
}

app.UseStaticFiles();
app.UseRouting();


app.MapControllerRoute(
    name: "default",
    pattern: "{controller}/{action=Index}/{id?}");

app.MapFallbackToFile("index.html"); ;

app.UseCors(config => config.WithOrigins("http://localhost:8000"));

app.UseSpa(config =>
{
    config.Options.SourcePath = "ClientApp";

    if (app.Environment.IsDevelopment())
        config.UseProxyToSpaDevelopmentServer("http://localhost:8000");
});

app.Run();
